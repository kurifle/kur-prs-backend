import datetime
from typing import Optional

from pydantic import BaseModel


class UserBase(BaseModel):
    id: str
    name: str
    role: str


class UserCreate(UserBase):
    pass


class UserUpdate(UserBase):
    pass


class User(UserBase):
    class Config:
        orm_mode = True


class EventBase(BaseModel):
    user_id: str
    title: str
    date: datetime.date
    slot: int
    type: str
    memo: Optional[str] = None


class EventCreate(EventBase):
    user_id: str


class EventUpdate(EventBase):
    pass


class Event(EventBase):
    id: int
    user: User

    class Config:
        orm_mode = True


class WeeklyEventCreate(BaseModel):
    user_id: str
    title: str
    type: str
    dfrom: datetime.date
    dto: datetime.date
    weekday: int
    slot: int
