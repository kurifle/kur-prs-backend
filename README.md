Practice Reservation System
===========================

Backend of practice reservation system.

## Features
- Simple and Lightweight
- Using Slack OAuth

## Dependencies
- Python >= 3.7
- fastapi
- SQLAlchemy
- pydantic
- slack-sdk
- uvicorn
- (optional) gunicorn

## Getting Started
### Installation
```
$ git clone https://gitlab.com/kurifle/kur-prs-backend.git
$ cd kur-prs-backend
$ poetry install
```

### Running locally
```
$ uvicorn kurprs.main:app --host 0.0.0.0
```
and visit `localhost:8000/docs/` in your web browser.
You will see the interactive API documentation.

## Usage
See interactive API docs.

## License
Copyright (c) 2021 Takayuki YANO

The source code is licensed under the MIT License, see LICENSE.