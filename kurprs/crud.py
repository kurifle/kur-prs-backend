import datetime

from sqlalchemy import and_, asc
from sqlalchemy.orm import Session

from . import models, schemas


def get_user(db: Session, user_id: str):
    """
    Get User by ID
    """
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    """
    Get User list (default: up to 100 user)
    """
    return (
        db.query(models.User)
        .order_by(asc(models.User.id))
        .offset(skip)
        .limit(limit)
        .all()
    )


def create_user(db: Session, user: schemas.UserCreate):
    """
    Create User
    """
    db_user = models.User(**user.dict())
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def update_user(db: Session, user: schemas.UserUpdate):
    """
    Update User Info
    """
    db_user = db.query(models.User).filter(models.User.id == user.id).one()
    db_user.name = user.name
    db_user.role = user.role
    db.commit()
    return db.query(models.User).filter(models.User.id == user.id).first()


def delete_user(db: Session, user_id: str):
    """
    Delete User by ID
    """
    # Delete all events of the user
    target_events = db.query(models.Event).filter(models.Event.user_id == user_id).all()
    for event in target_events:
        db.delete(event)
        db.commit()
    # Delete User
    target_user = db.query(models.User).filter(models.User.id == user_id).one()
    db.delete(target_user)
    db.commit()

    return target_user


def get_event(db: Session, event_id: int):
    """
    Get Event by ID
    """
    return db.query(models.Event).filter(models.Event.id == event_id).first()


def get_events_by_timeslot(db: Session, date: datetime.date, slot: int):
    """
    Get Events by timeslot
    """
    return (
        db.query(models.Event)
        .filter(models.Event.date == date)
        .filter(models.Event.slot == slot)
        .all()
    )


def get_user_event_by_timeslot(
    db: Session, user_id: str, date: datetime.date, slot: int
):
    """
    Get Event by user_id and timeslot
    """
    return (
        db.query(models.Event)
        .filter(models.Event.user_id == user_id)
        .filter(models.Event.date == date)
        .filter(models.Event.slot == slot)
        .first()
    )


def get_events(
    db: Session,
    skip: int = 0,
    limit: int = 1000,
    user_id: str = None,
    dfrom: datetime.date = datetime.date.today(),
    dto: datetime.date = datetime.date.today() + datetime.timedelta(days=31),
    slot: int = 0,
    type: str = None,
):
    """
    Get event list (default: up to 1000)
    """
    db_event = db.query(models.Event)
    if user_id:
        db_event = db_event.filter(models.Event.user_id == user_id)
    if dfrom:
        db_event = db_event.filter(models.Event.date >= dfrom)
    if dto:
        db_event = db_event.filter(models.Event.date <= dto)
    if slot:
        db_event = db_event.filter(models.Event.slot == slot)
    if type:
        db_event = db_event.filter(models.Event.type == type)
    return db_event.order_by(asc(models.Event.date)).offset(skip).limit(limit).all()


def get_events_by_type(
    db: Session,
    type: str,
    skip: int = 0,
    limit: int = 1000,
    dfrom: datetime.date = datetime.date.today(),
    dto: datetime.date = datetime.date.today() + datetime.timedelta(days=31),
):
    """
    Get Events by type
    """
    if dfrom and dto:
        return (
            db.query(models.Event)
            .filter(and_(models.Event.date <= dto, models.Event.date >= dfrom))
            .filter(models.Event.type == type)
            .order_by(asc(models.Event.id))
            .offset(skip)
            .limit(limit)
            .all()
        )
    return (
        db.query(models.Event)
        .filter(models.Event.type == type)
        .order_by(asc(models.Event.id))
        .offset(skip)
        .limit(limit)
        .all()
    )


def get_events_of_user(
    db: Session,
    user_id: str,
    dfrom: datetime.date = datetime.date.today(),
    dto: datetime.date = datetime.date.today() + datetime.timedelta(days=31),
):
    """
    Get Event list of User
    """
    if dfrom and dto:
        return (
            db.query(models.Event)
            .filter(models.Event.user_id == user_id)
            .filter(models.Event.date >= dfrom)
            .filter(models.Event.date <= dto)
            .order_by(asc(models.Event.id))
            .all()
        )
    return (
        db.query(models.Event)
        .filter(models.Event.user_id == user_id)
        .order_by(asc(models.Event.id))
        .all()
    )


def get_event_of_user(db: Session, user_id: str, event_id: int):
    """
    Get Event of User
    """
    return (
        db.query(models.Event)
        .filter(models.Event.user_id == user_id)
        .filter(models.Event.id == event_id)
        .first()
    )


def create_event(db: Session, event: schemas.EventCreate):
    """
    Create event
    """
    db_event = models.Event(**event.dict())
    db.add(db_event)
    db.commit()
    db.refresh(db_event)
    return db_event


def update_event(db: Session, event_id: int, event: schemas.EventUpdate):
    """
    Update Event
    """
    db_event = db.query(models.Event).filter(models.Event.id == event_id).one()
    db_event.title = event.title
    db_event.date = event.date
    db_event.slot = event.slot
    db_event.type = event.type
    db_event.memo = event.memo
    db.commit()
    return db.query(models.Event).filter(models.Event.id == event_id).first()


def delete_event(db: Session, event_id: int):
    """
    Delete Event
    """
    target_event = db.query(models.Event).filter(models.Event.id == event_id).one()
    db.delete(target_event)
    db.commit()
    return target_event


def delete_event_of_user(db: Session, user_id: str, event_id: int):
    """
    Delete Event
    """
    target_event = (
        db.query(models.Event)
        .filter(models.Event.user_id == user_id)
        .filter(models.Event.id == event_id)
        .one()
    )
    db.delete(target_event)
    db.commit()
    return target_event
