import datetime
from typing import List, Optional

import slack_sdk
from fastapi import Depends, FastAPI, Header, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.orm import Session

from . import crud, models, schemas
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

PRACTICE_MAX_NUM = 5
CHIEF_MAX_NUM = 1


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def slack_auth_test(access_token: str):
    client = slack_sdk.WebClient(access_token)
    try:
        response = client.auth_test()
    except slack_sdk.errors.SlackApiError:
        raise HTTPException(status_code=401)
    if response["ok"]:
        return response
    else:
        raise HTTPException(status_code=401)


"""
CRUD User
===========
POST /users/            --> add user
GET  /users/            --> list users
GET  /users/{user_id} --> show user info
PUT  /users/{user_id} --> update user
DEL  /users/{user_id} --> remove user and their events
"""


@app.post("/users/", response_model=schemas.User)
def create_user(
    user: schemas.UserCreate,
    access_token: str = Header(""),
    db: Session = Depends(get_db),
):
    """
    Create User
    """
    slack_auth_test(access_token)
    db_user = crud.get_user(db=db, user_id=user.id)
    if db_user:
        raise HTTPException(status_code=400, detail="User already registered")
    return crud.create_user(db=db, user=user)


@app.get("/users/", response_model=List[schemas.User])
def read_users(
    skip: int = 0,
    limit: int = 100,
    db: Session = Depends(get_db),
):
    """
    Get Users
    """
    db_users = crud.get_users(db=db, skip=skip, limit=limit)
    return db_users


@app.get("/users/{user_id}", response_model=schemas.User)
def read_user(user_id: str, db: Session = Depends(get_db)):
    """
    Get User by ID
    """
    db_user = crud.get_user(db=db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@app.put("/users/{user_id}", response_model=schemas.User)
def update_user(
    user_id: str,
    user: schemas.UserUpdate,
    access_token: str = Header(""),
    db: Session = Depends(get_db),
):
    """
    Update User
    """
    slack_auth_test(access_token)
    db_user = crud.get_user(db=db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return crud.update_user(db=db, user=user)


@app.delete("/users/{user_id}", response_model=schemas.User)
def delete_user(
    user_id: str, access_token: str = Header(""), db: Session = Depends(get_db)
):
    """
    Delete User
    """
    slack_auth_test(access_token)
    db_user = crud.get_user(db=db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    return crud.delete_user(db=db, user_id=user_id)


"""
CRUD Event
==========
POST /events/                           --> add event
GET  /events/                           --> list events
GET  /events/{event_id}                 --> show event info
GET  /users/{user_id}/events            --> list events
GET  /users/{user_id}/events/{event_id} --> show event info
PUT  /events/{event_id}                 --> update event
PUT  /users/{user_id}/events/{event_id} --> update event
DEL  /events/{event_id}                 --> remove event
DEL  /users/{user_id}/events/{event_id} --> remove event
"""


@app.post("/events/", response_model=schemas.Event)
def create_event(
    event: schemas.EventCreate,
    access_token: str = Header(""),
    db: Session = Depends(get_db),
):
    """
    Create Event
    """
    slack_auth_test(access_token)
    db_user = crud.get_user(db=db, user_id=event.user_id)
    if db_user is None:
        raise HTTPException(status_code=400, detail="User does NOT exist")

    if event.type == "practice" or event.type == "instruction":
        db_practice = crud.get_events(
            db=db, dfrom=event.date, dto=event.date, slot=event.slot, type="practice"
        )
        db_practice += crud.get_events(
            db=db,
            dfrom=event.date,
            dto=event.date,
            slot=event.slot,
            type="instruction",
        )
        if len(db_practice) >= PRACTICE_MAX_NUM:
            raise HTTPException(status_code=400, detail="Capacity Over")

    if event.type == "chief":
        db_chief = crud.get_events(
            db=db, dfrom=event.date, dto=event.date, slot=event.slot, type="chief"
        )
        if len(db_chief) >= CHIEF_MAX_NUM:
            raise HTTPException(status_code=400, detail="Capacity Over")

    db_event_user = crud.get_user_event_by_timeslot(
        db=db, user_id=event.user_id, date=event.date, slot=event.slot
    )
    if db_event_user:
        raise HTTPException(status_code=400, detail="Event already registered")

    return crud.create_event(db=db, event=event)


@app.post("/events/weekly", response_model=List[schemas.Event])
def create_weeklyevents(
    weekly_event: schemas.WeeklyEventCreate,
    access_token: str = Header(""),
    db: Session = Depends(get_db),
):
    """
    Create Weekly Events
    """
    slack_auth_test(access_token)
    db_user = crud.get_user(db=db, user_id=weekly_event.user_id)
    if db_user is None:
        raise HTTPException(status_code=400, detail="User does NOT exist")
    if weekly_event.dfrom > weekly_event.dto:
        raise HTTPException(status_code=400, detail="Invalid date range")

    days_ahead = weekly_event.weekday - weekly_event.dfrom.weekday()
    if days_ahead < 0:
        days_ahead += 7
    date = weekly_event.dfrom + datetime.timedelta(days_ahead)

    res = []

    while date < weekly_event.dto:
        db_event = crud.get_user_event_by_timeslot(
            db=db, user_id=weekly_event.user_id, date=date, slot=weekly_event.slot
        )
        if db_event:
            crud.delete_event(db=db, event_id=db_event.id)
        if weekly_event.type == "chief":
            db_events = crud.get_events(
                db=db,
                dfrom=date,
                dto=date,
                slot=weekly_event.slot,
                type=weekly_event.type,
            )
            for event in db_events:
                crud.delete_event(db, event_id=event.id)
        if weekly_event.type == "practice" or weekly_event.type == "instruction":
            db_events = crud.get_events(
                db=db, dfrom=date, dto=date, slot=weekly_event.slot, type="practice"
            )
            db_events += crud.get_events(
                db=db,
                dfrom=date,
                dto=date,
                slot=weekly_event.slot,
                type="instruction",
            )
            if len(db_events) >= PRACTICE_MAX_NUM:
                raise HTTPException(status_code=400, detail="Capacity Over")
        res.append(
            crud.create_event(
                db=db,
                event=schemas.EventCreate(
                    user_id=weekly_event.user_id,
                    title=weekly_event.title,
                    type=weekly_event.type,
                    date=date,
                    slot=weekly_event.slot,
                    memo="",
                ),
            )
        )
        date = date + datetime.timedelta(days=7)
    return res


@app.get("/events/", response_model=List[schemas.Event])
def read_events(
    skip: int = 0,
    limit: int = 1000,
    user_id: Optional[str] = None,
    dfrom: Optional[datetime.date] = None,
    dto: Optional[datetime.date] = None,
    slot: Optional[int] = None,
    type: Optional[str] = None,
    db: Session = Depends(get_db),
):
    """
    Get Events
    """
    return crud.get_events(
        db=db,
        skip=skip,
        limit=limit,
        user_id=user_id,
        dfrom=dfrom,
        dto=dto,
        slot=slot,
        type=type,
    )


@app.get("/events/{event_id}", response_model=schemas.Event)
def read_event(
    event_id: str,
    db: Session = Depends(get_db),
):
    """
    Get Event
    """
    db_event = crud.get_event(db=db, event_id=event_id)
    if db_event is None:
        raise HTTPException(status_code=404, detail="Event not found")
    return db_event


@app.get("/users/{user_id}/events", response_model=List[schemas.Event])
def read_user_events(
    user_id: str,
    dfrom: Optional[datetime.date] = datetime.date.today(),
    dto: Optional[datetime.date] = datetime.date.today() + datetime.timedelta(days=31),
    db: Session = Depends(get_db),
):
    """
    Get Events
    """
    db_events = crud.get_events_of_user(db=db, user_id=user_id, dfrom=dfrom, dto=dto)
    return db_events


@app.get("/users/{user_id}/events/{event_id}", response_model=schemas.Event)
def read_user_event(
    user_id: str,
    event_id: int,
    db: Session = Depends(get_db),
):
    """
    Get Event
    """
    db_event = crud.get_event_of_user(db=db, user_id=user_id, event_id=event_id)
    if db_event is None:
        raise HTTPException(status_code=404, detail="Event not found")
    return db_event


@app.put("/events/{event_id}", response_model=schemas.Event)
def update_event(
    event_id: int,
    event: schemas.EventUpdate,
    access_token: str = Header(""),
    db: Session = Depends(get_db),
):
    """
    Update Event
    """
    slack_auth_test(access_token)
    db_event = crud.get_event(db=db, event_id=event_id)
    if db_event is None:
        raise HTTPException(status_code=404, detail="Event not found")

    # db_user = crud.get_user(db=db, user_id=event.user_id)
    # if db_user is None:
    #   raise HTTPException(status_code=400, detail="User does NOT exist")

    db_same_event = crud.get_user_event_by_timeslot(
        db=db, user_id=db_event.user_id, date=event.date, slot=event.slot
    )
    if db_same_event:
        if db_same_event.id != event_id:
            raise HTTPException(status_code=400, detail="Event already registered")

    return crud.update_event(db=db, event_id=event_id, event=event)


@app.put("/users/{user_id}/events/{event_id}", response_model=schemas.Event)
def update_user_event(
    user_id: str,
    event_id: int,
    event: schemas.EventUpdate,
    access_token: str = Header(""),
    db: Session = Depends(get_db),
):
    """
    Update Event
    """
    slack_auth_test(access_token)
    db_event = crud.get_event_of_user(db=db, user_id=user_id, event_id=event_id)
    if db_event is None:
        raise HTTPException(status_code=404, detail="Event not found")

    db_same_event = crud.get_user_event_by_timeslot(
        db=db, user_id=event.user_id, date=event.date, slot=event.slot
    )
    if db_same_event:
        if db_same_event.id != event_id:
            raise HTTPException(status_code=400, detail="Event already registered")

    return crud.update_event(db=db, event_id=event_id, event=event)


@app.delete("/events/{event_id}")
def delete_event(
    event_id: int, access_token: str = Header(""), db: Session = Depends(get_db)
):
    """
    Delete Event
    """
    slack_auth_test(access_token)
    db_event = crud.get_event(db=db, event_id=event_id)
    if db_event is None:
        raise HTTPException(status_code=404, detail="Event not found")

    return crud.delete_event(db=db, event_id=event_id)


@app.delete("/users/{user_id}/events/{event_id}")
def delete_user_event(
    user_id: str,
    event_id: int,
    access_token: str = Header(""),
    db: Session = Depends(get_db),
):
    """
    Delete Event
    """
    slack_auth_test(access_token)
    db_event = crud.get_event_of_user(db=db, user_id=user_id, event_id=event_id)
    if db_event is None:
        raise HTTPException(status_code=404, detail="Event not found")

    return crud.delete_event(db=db, event_id=event_id)
